﻿using HarmonyLib;
using System;
using System.IO;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.GameMenus;
using TaleWorlds.CampaignSystem.MapEvents;
using TaleWorlds.CampaignSystem.Party;
using TaleWorlds.CampaignSystem.Roster;
using TaleWorlds.Core;
using TaleWorlds.Library;
using TaleWorlds.MountAndBlade;
using TaleWorlds.MountAndBlade.View.MissionViews;


namespace CampaignBattleMultiplayer
{
    public class SubModule : MBSubModuleBase
    {
        protected override void OnSubModuleLoad()
        {
            base.OnSubModuleLoad();

        }

        protected override void OnSubModuleUnloaded()
        {
            base.OnSubModuleUnloaded();

        }

        protected override void OnBeforeInitialModuleScreenSetAsRoot()
        {
            base.OnBeforeInitialModuleScreenSetAsRoot();


        }

        protected override void InitializeGameStarter(Game game, IGameStarter starterObject)
        {
            base.InitializeGameStarter(game, starterObject);

            if (starterObject is CampaignGameStarter cStarter)
            {
                cStarter.AddBehavior(new CampaignBattleMultiplayer_CampaignBehavior());
            }
        }

        public override void OnMissionBehaviorInitialize(Mission mission)
        {
            base.OnMissionBehaviorInitialize(mission);

            mission.AddMissionBehavior(new CampaignBattleMultiplayer_MissionView());
        }
    }
    public class CampaignBattleMultiplayer_MissionView : MissionView
    {
        public override void OnMissionScreenTick(float dt)
        {
            base.OnMissionScreenTick(dt);
        }
    }

    public class CampaignBattleMultiplayer_CampaignBehavior : CampaignBehaviorBase
    {
        private PartyBase? partyBase1;
        private PartyBase? partyBase2;
        private MapEvent? eventOccur;

        public override void RegisterEvents()
        {
            CampaignEvents.MapEventStarted.AddNonSerializedListener(this, EncounterPartyEvent);
            //CampaignEvents.OnMissionStartedEvent.AddNonSerializedListener(this, SaveArmyConfiguration);
            CampaignEvents.OnGameLoadedEvent.AddNonSerializedListener(this, CreateModMenu);
        }

        private void CreateModMenu(CampaignGameStarter starter)
        {
            starter.AddGameMenuOption("encounter", "export_data",
                "Export data for multiplayer battle", new GameMenuOption.OnConditionDelegate(this.ExportDataCondition),
                new GameMenuOption.OnConsequenceDelegate(this.ExportDataConsequence));
            starter.AddGameMenuOption("encounter", "import_data",
                "Import data from multiplayer battle", new GameMenuOption.OnConditionDelegate(this.ImportDataCondition),
                new GameMenuOption.OnConsequenceDelegate(this.ImportDataConsequence));
        }

        private bool ImportDataCondition(MenuCallbackArgs args)
        {
            return true;
        }

        private void ImportDataConsequence(MenuCallbackArgs args)
        {
            if (partyBase1 == null || partyBase2 == null)
                return;

            if (partyBase1.General.IsPlayerCharacter == false && partyBase2.General.IsPlayerCharacter == false)
                return;

            PartyBase playerParty = partyBase1.General.IsPlayerCharacter == true ? partyBase1 : partyBase2;

            string ennemy_name = partyBase1.General.IsPlayerCharacter == true ? partyBase2.Name.ToString() : partyBase1.Name.ToString();

            int menCount = 0;
            foreach (var item in partyBase1.MemberRoster.GetTroopRoster())
            {
                menCount += item.Number;
            }
            foreach (var item in partyBase2.MemberRoster.GetTroopRoster())
            {
                menCount += item.Number;
            }

            string path = "C:\\Users\\Shadow\\Documents\\Mods\\import_" + playerParty.Name.ToString() + "_vs_" + ennemy_name + "_" + menCount + ".Army";

            try
            {
                StreamReader sr = new(path);
                GeneratImportPlayer(sr, partyBase1.General.IsPlayerCharacter == false ? ref partyBase2 : ref partyBase1);
                GenerateImportIA(sr, partyBase1.General.IsPlayerCharacter == true ? ref partyBase2 : ref partyBase1);
                sr.Close();
            }
            catch (Exception) { }
        }

        private void GenerateImportIA(StreamReader srIa, PartyBase IAParty)
        {
            var playerRoster = IAParty.MemberRoster.GetTroopRoster();
            //srPlayer.ReadLine("#### Start player army composition ####");
            srIa.ReadLine();

            String ?line = srIa.ReadLine();

            while (line != null && line.Contains("#### End IA army composition ####") == false)
            {
                string[] parsedLine = line.Split('=');

                foreach (TroopRosterElement item in playerRoster)
                {
                    if (item.Character.Name.ToString().Contains(parsedLine[0]) == true)
                    {
                        int idx = IAParty.MemberRoster.FindIndexOfTroop(item.Character);
                        IAParty.MemberRoster.SetElementNumber(idx, int.Parse(parsedLine[1].Split(' ')[0]));
                        IAParty.MemberRoster.SetElementWoundedNumber(idx, int.Parse(parsedLine[2]));
                    }
                }

                line = srIa.ReadLine();
            }
            srIa.ReadLine();

            var IaGeneral = IAParty.General;
            //srPlayer.ReadLine("#### Start player general skill ####");
            srIa.ReadLine();

            line = srIa.ReadLine();
            while (line != null && line.Contains("#### End IA general skill ####") == false)
            {
                string[] parsedLine = line.Split('=');

                line = srIa.ReadLine();
                /*srPlayer.WriteLine("One_handed=" + playerGeneral.GetSkillValue(DefaultSkills.OneHanded));
                 srPlayer.WriteLine("Two_handed=" + playerGeneral.GetSkillValue(DefaultSkills.TwoHanded));
                srPlayer.WriteLine("Polearm=" + playerGeneral.GetSkillValue(DefaultSkills.Polearm));

                srPlayer.WriteLine("Bow=" + playerGeneral.GetSkillValue(DefaultSkills.Bow));
                srPlayer.WriteLine("Crossbow=" + playerGeneral.GetSkillValue(DefaultSkills.Crossbow));
                srPlayer.WriteLine("Throwing=" + playerGeneral.GetSkillValue(DefaultSkills.Throwing));

                srPlayer.WriteLine("Riding=" + playerGeneral.GetSkillValue(DefaultSkills.Riding));
                srPlayer.WriteLine("Athletics=" + playerGeneral.GetSkillValue(DefaultSkills.Athletics));
                srPlayer.WriteLine("Smithing=" + playerGeneral.GetSkillValue(DefaultSkills.Crafting));

                srPlayer.WriteLine("Scouting=" + playerGeneral.GetSkillValue(DefaultSkills.Scouting));
                srPlayer.WriteLine("Tactics=" + playerGeneral.GetSkillValue(DefaultSkills.Tactics));
                srPlayer.WriteLine("Roguery=" + playerGeneral.GetSkillValue(DefaultSkills.Roguery));

                srPlayer.WriteLine("Charm=" + playerGeneral.GetSkillValue(DefaultSkills.Charm));
                srPlayer.WriteLine("Leadership=" + playerGeneral.GetSkillValue(DefaultSkills.Leadership));
                srPlayer.WriteLine("Trade=" + playerGeneral.GetSkillValue(DefaultSkills.Trade));

                srPlayer.WriteLine("Steward=" + playerGeneral.GetSkillValue(DefaultSkills.Steward));
                srPlayer.WriteLine("Medicine=" + playerGeneral.GetSkillValue(DefaultSkills.Medicine));
                srPlayer.WriteLine("Engineering=" + playerGeneral.GetSkillValue(DefaultSkills.Engineering));
                */
            }
        }

        private void GeneratImportPlayer(StreamReader srPlayer, PartyBase playerParty)
        {
            var playerRoster = playerParty.MemberRoster.GetTroopRoster();
            //srPlayer.ReadLine("#### Start player army composition ####");
            srPlayer.ReadLine();

            String ?line = srPlayer.ReadLine();

            while (line != null && line.Contains("#### End player army composition ####") == false)
            {
                string[] parsedLine = line.Split('=');

                foreach (TroopRosterElement item in playerRoster)
                {
                    if (item.Character.Name.ToString().Contains(parsedLine[0]) == true)
                    {
                        int idx = playerParty.MemberRoster.FindIndexOfTroop(item.Character);
                        playerParty.MemberRoster.SetElementNumber(idx, int.Parse(parsedLine[1].Split(' ')[0]));
                        playerParty.MemberRoster.SetElementWoundedNumber(idx, int.Parse(parsedLine[2]));
                    }
                }

                line = srPlayer.ReadLine();
            }
            srPlayer.ReadLine();

            var playerGeneral = playerParty.General;
            //srPlayer.ReadLine("#### Start player general skill ####");
            srPlayer.ReadLine();

            line = srPlayer.ReadLine();
            while (line != null && line.Contains("#### End player general skill ####") == false)
            {
                string[] parsedLine = line.Split('=');

                line = srPlayer.ReadLine();
                /*srPlayer.WriteLine("One_handed=" + playerGeneral.GetSkillValue(DefaultSkills.OneHanded));
                srPlayer.WriteLine("Two_handed=" + playerGeneral.GetSkillValue(DefaultSkills.TwoHanded));
                srPlayer.WriteLine("Polearm=" + playerGeneral.GetSkillValue(DefaultSkills.Polearm));

                srPlayer.WriteLine("Bow=" + playerGeneral.GetSkillValue(DefaultSkills.Bow));
                srPlayer.WriteLine("Crossbow=" + playerGeneral.GetSkillValue(DefaultSkills.Crossbow));
                srPlayer.WriteLine("Throwing=" + playerGeneral.GetSkillValue(DefaultSkills.Throwing));

                srPlayer.WriteLine("Riding=" + playerGeneral.GetSkillValue(DefaultSkills.Riding));
                srPlayer.WriteLine("Athletics=" + playerGeneral.GetSkillValue(DefaultSkills.Athletics));
                srPlayer.WriteLine("Smithing=" + playerGeneral.GetSkillValue(DefaultSkills.Crafting));

                srPlayer.WriteLine("Scouting=" + playerGeneral.GetSkillValue(DefaultSkills.Scouting));
                srPlayer.WriteLine("Tactics=" + playerGeneral.GetSkillValue(DefaultSkills.Tactics));
                srPlayer.WriteLine("Roguery=" + playerGeneral.GetSkillValue(DefaultSkills.Roguery));

                srPlayer.WriteLine("Charm=" + playerGeneral.GetSkillValue(DefaultSkills.Charm));
                srPlayer.WriteLine("Leadership=" + playerGeneral.GetSkillValue(DefaultSkills.Leadership));
                srPlayer.WriteLine("Trade=" + playerGeneral.GetSkillValue(DefaultSkills.Trade));

                srPlayer.WriteLine("Steward=" + playerGeneral.GetSkillValue(DefaultSkills.Steward));
                srPlayer.WriteLine("Medicine=" + playerGeneral.GetSkillValue(DefaultSkills.Medicine));
                srPlayer.WriteLine("Engineering=" + playerGeneral.GetSkillValue(DefaultSkills.Engineering));
                */
            }
        }

        private void ExportDataConsequence(MenuCallbackArgs args)
        {
            ExportArmyBattleConfiguration();
        }
        private bool ExportDataCondition(MenuCallbackArgs args)
        {
            return true;
        }

        private void EncounterPartyEvent(MapEvent @event, PartyBase base1, PartyBase base2)
        {
            partyBase1 = base1;
            partyBase2 = base2;
            eventOccur = @event;
            InformationManager.DisplayMessage(new InformationMessage("Encounter : " + base1.Name + " ," + base2.Name));
        }

        private void ExportArmyBattleConfiguration()
        {
            if (partyBase1 == null || partyBase2 == null)
                return;

            if (partyBase1.General.IsPlayerCharacter == false && partyBase2.General.IsPlayerCharacter == false)
                return;

            PartyBase playerParty = partyBase1.General.IsPlayerCharacter == true ? partyBase1 : partyBase2;

            string ennemy_name = partyBase1.General.IsPlayerCharacter == true ? partyBase2.Name.ToString() : partyBase1.Name.ToString();

            int menCount = 0;
            foreach (var item in partyBase1.MemberRoster.GetTroopRoster())
            {
                menCount += item.Number;
            }
            foreach (var item in partyBase2.MemberRoster.GetTroopRoster())
            {
                menCount += item.Number;
            }

            try
            {
                string path = "C:\\Users\\Shadow\\Documents\\Mods\\export_" + playerParty.Name.ToString() + "_vs_" + ennemy_name + "_" + menCount + ".Army";

                StreamWriter sw = new(path);
                GenerateExportPlayer(sw, partyBase1.General.IsPlayerCharacter == false ? partyBase2 : partyBase1);
                GenerateExportIA(sw, partyBase1.General.IsPlayerCharacter == true ? partyBase2 : partyBase1);
                sw.Close();
            }
            catch(Exception) { }
        }

        private void GenerateExportIA(StreamWriter swIA, PartyBase IAParty)
        {
            var IARoster = IAParty.MemberRoster.GetTroopRoster();

            swIA.WriteLine("#### Start IA army composition ####");
            foreach (var item in IARoster)
            {
                if (IAParty.General != null)
                {
                    if (item.Character.Name == IAParty.General.Name)
                        continue;
                }
                //InformationManager.DisplayMessage(new InformationMessage("Unite : " + item.Character.Name + " nombre : " + item.Number));
                swIA.WriteLine(item.Character.Name + "=" + item.Number + " Wounded=" + item.WoundedNumber);
            }
            swIA.WriteLine("#### End IA army composition ####");
            swIA.WriteLine("");

            if (IAParty.General == null)
                return;

            swIA.WriteLine("#### Start IA general equipment ####");

            var IAEquipment = IAParty.General.Equipment;
            if (IAEquipment.GetEquipmentFromSlot(EquipmentIndex.Weapon0).Item != null)
                swIA.WriteLine("Weapon0=" + IAEquipment.GetEquipmentFromSlot(EquipmentIndex.Weapon0).Item.Name);
            if (IAEquipment.GetEquipmentFromSlot(EquipmentIndex.Weapon1).Item != null)
                swIA.WriteLine("Weapon1=" + IAEquipment.GetEquipmentFromSlot(EquipmentIndex.Weapon1).Item.Name);
            if (IAEquipment.GetEquipmentFromSlot(EquipmentIndex.Weapon2).Item != null)
                swIA.WriteLine("Weapon2=" + IAEquipment.GetEquipmentFromSlot(EquipmentIndex.Weapon2).Item.Name);
            if (IAEquipment.GetEquipmentFromSlot(EquipmentIndex.Weapon3).Item != null)
                swIA.WriteLine("Weapon3=" + IAEquipment.GetEquipmentFromSlot(EquipmentIndex.Weapon3).Item.Name);
            if (IAEquipment.GetEquipmentFromSlot(EquipmentIndex.Head).Item != null)
                swIA.WriteLine("Head=" + IAEquipment.GetEquipmentFromSlot(EquipmentIndex.Head).Item.Name);
            if (IAEquipment.GetEquipmentFromSlot(EquipmentIndex.Cape).Item != null)
                swIA.WriteLine("Cape=" + IAEquipment.GetEquipmentFromSlot(EquipmentIndex.Cape).Item.Name);
            if (IAEquipment.GetEquipmentFromSlot(EquipmentIndex.Body).Item != null)
                swIA.WriteLine("Body=" + IAEquipment.GetEquipmentFromSlot(EquipmentIndex.Body).Item.Name);
            if (IAEquipment.GetEquipmentFromSlot(EquipmentIndex.Gloves).Item != null)
                swIA.WriteLine("Gloves=" + IAEquipment.GetEquipmentFromSlot(EquipmentIndex.Gloves).Item.Name);
            if (IAEquipment.GetEquipmentFromSlot(EquipmentIndex.Leg).Item != null)
                swIA.WriteLine("Leg=" + IAEquipment.GetEquipmentFromSlot(EquipmentIndex.Leg).Item.Name);

            if (IAEquipment.GetEquipmentFromSlot(EquipmentIndex.HorseHarness).Item != null)
                swIA.WriteLine("HorseHarness=" + IAEquipment.GetEquipmentFromSlot(EquipmentIndex.HorseHarness).Item.Name);
            if (IAEquipment.GetEquipmentFromSlot(EquipmentIndex.Horse).Item != null)
                swIA.WriteLine("Horse=" + IAEquipment.GetEquipmentFromSlot(EquipmentIndex.Horse).Item.Name);

            if (IAEquipment.GetEquipmentFromSlot(EquipmentIndex.ExtraWeaponSlot).Item != null)
                swIA.WriteLine("ExtraWeaponSlot=" + IAEquipment.GetEquipmentFromSlot(EquipmentIndex.ExtraWeaponSlot).Item.Name);

            swIA.WriteLine("#### End IA general equipment ####");
            swIA.WriteLine("");

            var IAGeneral = IAParty.General;
            swIA.WriteLine("#### Start IA general information ####");

            swIA.WriteLine("FirstName=" + IAParty.LeaderHero.FirstName);

            swIA.WriteLine("LastName=" + IAParty.LeaderHero.Name);
            swIA.WriteLine("Gender=" + (IAGeneral.IsFemale == true ? "Female" : "Male"));
            swIA.WriteLine("hitpoint=" + IAGeneral.HitPoints);
            swIA.WriteLine("Culture=" + IAParty.LeaderHero.Culture);

            swIA.WriteLine("#### End IA general information ####");
            swIA.WriteLine("");

            swIA.WriteLine("#### Start IA general skill ####");

            swIA.WriteLine("One_handed=" + IAGeneral.GetSkillValue(DefaultSkills.OneHanded));
            swIA.WriteLine("Two_handed=" + IAGeneral.GetSkillValue(DefaultSkills.TwoHanded));
            swIA.WriteLine("Polearm=" + IAGeneral.GetSkillValue(DefaultSkills.Polearm));

            swIA.WriteLine("Bow=" + IAGeneral.GetSkillValue(DefaultSkills.Bow));
            swIA.WriteLine("Crossbow=" + IAGeneral.GetSkillValue(DefaultSkills.Crossbow));
            swIA.WriteLine("Throwing=" + IAGeneral.GetSkillValue(DefaultSkills.Throwing));

            swIA.WriteLine("Riding=" + IAGeneral.GetSkillValue(DefaultSkills.Riding));
            swIA.WriteLine("Athletics=" + IAGeneral.GetSkillValue(DefaultSkills.Athletics));
            swIA.WriteLine("Smithing=" + IAGeneral.GetSkillValue(DefaultSkills.Crafting));

            swIA.WriteLine("Scouting=" + IAGeneral.GetSkillValue(DefaultSkills.Scouting));
            swIA.WriteLine("Tactics=" + IAGeneral.GetSkillValue(DefaultSkills.Tactics));
            swIA.WriteLine("Roguery=" + IAGeneral.GetSkillValue(DefaultSkills.Roguery));

            swIA.WriteLine("Charm=" + IAGeneral.GetSkillValue(DefaultSkills.Charm));
            swIA.WriteLine("Leadership=" + IAGeneral.GetSkillValue(DefaultSkills.Leadership));
            swIA.WriteLine("Trade=" + IAGeneral.GetSkillValue(DefaultSkills.Trade));

            swIA.WriteLine("Steward=" + IAGeneral.GetSkillValue(DefaultSkills.Steward));
            swIA.WriteLine("Medicine=" + IAGeneral.GetSkillValue(DefaultSkills.Medicine));
            swIA.WriteLine("Engineering=" + IAGeneral.GetSkillValue(DefaultSkills.Engineering));

            swIA.WriteLine("#### End IA general skill ####");
            swIA.WriteLine("");
        }

        private void GenerateExportPlayer(StreamWriter swPlayer, PartyBase playerParty)
        {
            var playerRoster = playerParty.MemberRoster.GetTroopRoster();
            swPlayer.WriteLine("#### Start player army composition ####");
            var it = playerRoster[0];

            foreach (var item in playerRoster)
            {
                if (item.Character.Name == playerParty.General.Name)
                    continue;
                //InformationManager.DisplayMessage(new InformationMessage("Unite : " + item.Character.Name + " nombre : " + item.Number));
                swPlayer.WriteLine(item.Character.Name + "=" + item.Number + " Wounded=" + item.WoundedNumber);
            }
            swPlayer.WriteLine("#### End player army composition ####");
            swPlayer.WriteLine("");

            swPlayer.WriteLine("#### Start player general equipment ####");

            var playerEquipment = playerParty.General.Equipment;
            swPlayer.Write("Weapon0=");
            if (playerEquipment.GetEquipmentFromSlot(EquipmentIndex.Weapon0).Item != null)
                swPlayer.WriteLine(playerEquipment.GetEquipmentFromSlot(EquipmentIndex.Weapon0).Item.Name);
            else
                swPlayer.WriteLine("None");

            swPlayer.Write("Weapon1=");
            if (playerEquipment.GetEquipmentFromSlot(EquipmentIndex.Weapon1).Item != null)
                swPlayer.WriteLine(playerEquipment.GetEquipmentFromSlot(EquipmentIndex.Weapon1).Item.Name);
            else
                swPlayer.WriteLine("None");

            swPlayer.Write("Weapon2=");
            if (playerEquipment.GetEquipmentFromSlot(EquipmentIndex.Weapon2).Item != null)
                swPlayer.WriteLine(playerEquipment.GetEquipmentFromSlot(EquipmentIndex.Weapon2).Item.Name);
            else
                swPlayer.WriteLine("None");

            swPlayer.Write("Weapon3=");
            if (playerEquipment.GetEquipmentFromSlot(EquipmentIndex.Weapon3).Item != null)
                swPlayer.WriteLine(playerEquipment.GetEquipmentFromSlot(EquipmentIndex.Weapon3).Item.Name);
            else
                swPlayer.WriteLine("None");

            swPlayer.Write("Head=");
            if (playerEquipment.GetEquipmentFromSlot(EquipmentIndex.Head).Item != null)
                swPlayer.WriteLine(playerEquipment.GetEquipmentFromSlot(EquipmentIndex.Head).Item.Name);
            else
                swPlayer.WriteLine("None");

            swPlayer.Write("Cape=");
            if (playerEquipment.GetEquipmentFromSlot(EquipmentIndex.Cape).Item != null)
                swPlayer.WriteLine(playerEquipment.GetEquipmentFromSlot(EquipmentIndex.Cape).Item.Name);
            else
                swPlayer.WriteLine("None");

            swPlayer.Write("Body=");
            if (playerEquipment.GetEquipmentFromSlot(EquipmentIndex.Body).Item != null)
                swPlayer.WriteLine(playerEquipment.GetEquipmentFromSlot(EquipmentIndex.Body).Item.Name);
            else
                swPlayer.WriteLine("None");

            swPlayer.Write("Gloves=");
            if (playerEquipment.GetEquipmentFromSlot(EquipmentIndex.Gloves).Item != null)
                swPlayer.WriteLine("Gloves=" + playerEquipment.GetEquipmentFromSlot(EquipmentIndex.Gloves).Item.Name);
            else
                swPlayer.WriteLine("None");

            swPlayer.Write("Leg=");
            if (playerEquipment.GetEquipmentFromSlot(EquipmentIndex.Leg).Item != null)
                swPlayer.WriteLine("Leg=" + playerEquipment.GetEquipmentFromSlot(EquipmentIndex.Leg).Item.Name);
            else
                swPlayer.WriteLine("None");

            swPlayer.Write("HorseHarness=");
            if (playerEquipment.GetEquipmentFromSlot(EquipmentIndex.HorseHarness).Item != null)
                swPlayer.WriteLine("HorseHarness=" + playerEquipment.GetEquipmentFromSlot(EquipmentIndex.HorseHarness).Item.Name);
            else
                swPlayer.WriteLine("None");

            swPlayer.Write("Horse=");
            if (playerEquipment.GetEquipmentFromSlot(EquipmentIndex.Horse).Item != null)
                swPlayer.WriteLine("Horse=" + playerEquipment.GetEquipmentFromSlot(EquipmentIndex.Horse).Item.Name);
            else
                swPlayer.WriteLine("None");

            swPlayer.Write("ExtraWeaponSlot=");
            if (playerEquipment.GetEquipmentFromSlot(EquipmentIndex.ExtraWeaponSlot).Item != null)
                swPlayer.WriteLine("ExtraWeaponSlot=" + playerEquipment.GetEquipmentFromSlot(EquipmentIndex.ExtraWeaponSlot).Item.Name);
            else
                swPlayer.WriteLine("None");

            swPlayer.WriteLine("#### End player general equipment ####");
            swPlayer.WriteLine("");

            var playerGeneral = playerParty.General;
            swPlayer.WriteLine("#### Start player general information ####");

            swPlayer.WriteLine("FirstName=" + playerParty.LeaderHero.FirstName);

            swPlayer.WriteLine("LastName=" + playerParty.LeaderHero.Name);
            swPlayer.WriteLine("Gender=" + (playerGeneral.IsFemale == true ? "Female" : "Male"));
            swPlayer.WriteLine("hitpoint=" + playerGeneral.HitPoints);
            swPlayer.WriteLine("Culture=" + playerParty.LeaderHero.Culture);

            swPlayer.WriteLine("#### End player general information ####");
            swPlayer.WriteLine("");

            swPlayer.WriteLine("#### Start player general skill ####");

            swPlayer.WriteLine("One_handed=" + playerGeneral.GetSkillValue(DefaultSkills.OneHanded));
            swPlayer.WriteLine("Two_handed=" + playerGeneral.GetSkillValue(DefaultSkills.TwoHanded));
            swPlayer.WriteLine("Polearm=" + playerGeneral.GetSkillValue(DefaultSkills.Polearm));

            swPlayer.WriteLine("Bow=" + playerGeneral.GetSkillValue(DefaultSkills.Bow));
            swPlayer.WriteLine("Crossbow=" + playerGeneral.GetSkillValue(DefaultSkills.Crossbow));
            swPlayer.WriteLine("Throwing=" + playerGeneral.GetSkillValue(DefaultSkills.Throwing));

            swPlayer.WriteLine("Riding=" + playerGeneral.GetSkillValue(DefaultSkills.Riding));
            swPlayer.WriteLine("Athletics=" + playerGeneral.GetSkillValue(DefaultSkills.Athletics));
            swPlayer.WriteLine("Smithing=" + playerGeneral.GetSkillValue(DefaultSkills.Crafting));

            swPlayer.WriteLine("Scouting=" + playerGeneral.GetSkillValue(DefaultSkills.Scouting));
            swPlayer.WriteLine("Tactics=" + playerGeneral.GetSkillValue(DefaultSkills.Tactics));
            swPlayer.WriteLine("Roguery=" + playerGeneral.GetSkillValue(DefaultSkills.Roguery));

            swPlayer.WriteLine("Charm=" + playerGeneral.GetSkillValue(DefaultSkills.Charm));
            swPlayer.WriteLine("Leadership=" + playerGeneral.GetSkillValue(DefaultSkills.Leadership));
            swPlayer.WriteLine("Trade=" + playerGeneral.GetSkillValue(DefaultSkills.Trade));

            swPlayer.WriteLine("Steward=" + playerGeneral.GetSkillValue(DefaultSkills.Steward));
            swPlayer.WriteLine("Medicine=" + playerGeneral.GetSkillValue(DefaultSkills.Medicine));
            swPlayer.WriteLine("Engineering=" + playerGeneral.GetSkillValue(DefaultSkills.Engineering));

            swPlayer.WriteLine("#### End player general skill ####");

            swPlayer.WriteLine("");
        }

        public override void SyncData(IDataStore dataStore)
        {
            //throw new NotImplementedException();
        }
    }
}